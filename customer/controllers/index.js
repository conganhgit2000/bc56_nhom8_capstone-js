// ALL chức năng Api của ADmin
function fetchProductList() {
  axios({
    url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/project",
    method: "GET",
  })
    .then(function (res) {
      renderDSSP(res.data);
      console.log("res132:", res.data);
    })
    .catch(function (err) {});
}

fetchProductList();

function xoaSp(id) {
  axios({
    url: `https://64c8b67ca1fe0128fbd61a36.mockapi.io/project/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchProductList();
      onSuccess("Xóa thành công");
    })
    .catch(function (err) {});
}

function themSp() {
  var sp = layThongTinTuForm();
  //kiểm tra không được bỏ trống
  var valid =
    validation.kiemTraRong(sp.name, "phoneName") &
    validation.kiemTraRong(sp.price, "price") &
    validation.kiemTraRong(sp.screen, "screen") &
    validation.kiemTraRong(sp.backCamera, "backCamera") &
    validation.kiemTraRong(sp.frontCamera, "frontCamera") &
    validation.kiemTraRong(sp.img, "imgLink") &
    validation.kiemTraRong(sp.desc, "description");

  // kiểm tra tất cả phải là số
  var valid = validation.kiemTraNumber(sp.price, "price");

  // kiểm tra thương hiệu
  var valid = validation.kiemTraThuongHieu(sp.type, "brand");

  if (!valid) {
    return;
  }

  axios({
    url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/project",
    method: "POST",
    data: sp,
  })
    .then(function (res) {
      console.log("res:", res);
      $("#myModal").modal("hide");
      fetchProductList();
      onSuccess("Thêm sp thành công");
    })
    .catch(function (err) {
      console.log("themSp ~ err:", err);
    });
}

function suaSp(id) {
  console.log("suaSp ~ id:", id);
  axios({
    url: `https://64c8b67ca1fe0128fbd61a36.mockapi.io/project/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res:", res);
      $("#myModal").modal("show");
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {});
  document.getElementById("MaSP").disabled = true;
}

function capNhapSp() {
  var sp = layThongTinTuForm();
  console.log("capNhapSp ~ sp:", sp);
  axios({
    url: `https://64c8b67ca1fe0128fbd61a36.mockapi.io/project/${sp.id}`,
    method: "PUT",
    data: sp,
  })
    .then(function (res) {
      //lấy lại danh sách sản phẩm sau khi cập nhập thành công
      fetchProductList();
      onSuccess("Cập nhập thành công");
      $("#myModal").modal("hide");
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function reset() {
  // productForm - ID của thẻ form chứa các input cần reset
  document.getElementById("productForm").reset();
}

document.querySelector("#keyword").oninput = function (event) {
  var tuKhoa = event.target.value;
  var arrSearch = [];

  axios({
    url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/project",
    method: "GET",
  })
    .then(function (res) {
      for (index = 0; index < res.data.length; index++) {
        var namePhone = res.data[index].name;

        tuKhoa = stringToSlug(tuKhoa); //đổi từ chữ HOA --> thường
        namePhone = stringToSlug(namePhone); //đổi từ chữ HOA --> thường

        if (namePhone.search(tuKhoa) !== -1) {
          arrSearch.push(res.data[index]);
        }
      }
      renderDSSP(arrSearch);
    })
    .catch(function (err) {});
};

function sapXepGia() {
  var trData = document.querySelector("tr[data-title]");
  axios({
    url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/project",
    method: "GET",
  })
    .then(function (res) {
      var order = trData.getAttribute("data-order");
      var arrSapXep = _.orderBy(res.data, ["price"], [order]);
      if (order == "asc") {
        trData.setAttribute("data-order", "desc");
      } else {
        trData.setAttribute("data-order", "asc");
      }
      renderDSSP(arrSapXep);
    })
    .catch(function (err) {});
}

// ALL chức năng của User
function fetchProductListUser() {
  axios({
    url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/project",
    method: "GET",
  })
    .then(function (res) {
      renderSpUser(res.data);
      console.log("resUSER:", res.data);
    })
    .catch(function (err) {});
}
fetchProductListUser();
