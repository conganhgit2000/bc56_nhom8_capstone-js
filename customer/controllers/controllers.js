// render danh sách Admin
function renderDSSP(productList) {
  var contentHTML = "";
  for (var i = 0; i < productList.length; i++) {
    product = productList[i];
    var contentTr = `<tr>
                          <td>${product.id}</td>
                          <td>${product.name}</td>
                          <td>${product.price}</td>
                          <td><img style ="width:300px; height:100px" src=${product.img} alt="" /></td>
                          <td>${product.desc}</td>
                          <td>
                              <button class="btn btn-danger" onclick=xoaSp(${product.id})>Xóa</button>
                              <button class="btn btn-warning" onclick=suaSp(${product.id})>Sửa</button>
                          </td>
                      </tr>`;
    contentHTML += contentTr;
  }
  //dom
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function renderSpUser(productListUser) {
  var contentHTMLdiv = "";
  for (var i = 0; i < productListUser.length; i++) {
    product = productListUser[i];
    var contentDiv = `
                      <div class="col-sm-6 col-lg-4 bg_card">
                        <div class="card card_item">
                          <img
                            src=${product.img}
                            class="card-img-top"
                            alt="..."
                          />
                          <div class="card_item_body">
                            <h4>${product.name}</h4>
                            <p>${product.price}$</p>
                            <p class="card_brand">${product.type}</p>
                            <p class="card_desc"><b>Description: </b>${product.desc}</p>
                          </div>
                          <div class="card_overlay">
                            <h2>Specifications</h2>
                            <div class="card_text">
                              <p><b>Screen:</b> ${product.screen}</p>
                              <p><b>Back camera:</b> ${product.backCamera}</p>
                              <p><b>Front camera:</b> ${product.frontCamera}</p>
                            </div>
                            <div class="card_link">
                              <a href="">click here for more details</a>
                            </div>
                            <button class="btn btn-warning">Thêm vào giỏ hàng</button>
                          </div>
                        </div>
                      </div>`;
    contentHTMLdiv += contentDiv;
  }

  document.getElementById("renderDanhSachUser").innerHTML = contentHTMLdiv;
}

function layThongTinTuForm() {
  var id = document.getElementById("MaSP").value;
  var name = document.getElementById("phoneName").value;
  var price = document.getElementById("price").value;
  var screen = document.getElementById("screen").value;
  var backCamera = document.getElementById("backCamera").value;
  var frontCamera = document.getElementById("frontCamera").value;
  var imgLink = document.getElementById("imgLink").value;
  var description = document.getElementById("description").value;
  var brand = document.getElementById("brand").value;
  return {
    id: id,
    name: name,
    price: price,
    screen: screen,
    backCamera: backCamera,
    frontCamera: frontCamera,
    img: imgLink,
    desc: description,
    type: brand,
  };
}

function showThongTinLenForm(data) {
  document.getElementById("MaSP").value = data.id;
  document.getElementById("phoneName").value = data.name;
  document.getElementById("price").value = data.price;
  document.getElementById("screen").value = data.screen;
  document.getElementById("backCamera").value = data.backCamera;
  document.getElementById("frontCamera").value = data.frontCamera;
  document.getElementById("imgLink").value = data.img;
  document.getElementById("description").value = data.desc;
  document.getElementById("imgLink").value = data.img;
  document.getElementById("brand").value = data.type;
}

function onSuccess(message) {
  Swal.fire(message, "", "success");
}
